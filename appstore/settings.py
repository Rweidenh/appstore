"""
Django settings for appstore project.

Generated by 'django-admin startproject' using Django 2.1.7.

For more information on this file, see
https://docs.djangoproject.com/en/2.1/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/2.1/ref/settings/
"""
from os.path import join as filejoin
from decouple import config, Csv, UndefinedValueError
import os

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/2.1/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = config('SECRET_KEY',default="XXXX")

# SECURITY WARNING: don't run with debug turned on in production!
#DEBUG=config('DEBUG', default=True)

DEBUG=True

# Navigation Bar settings
BIOVIZ_REPOSITORY = config('BIOVIZ_REPOSITORY', default='https://bitbucket.org/lorainelab/bioviz/')
BIOVIZ_BRANCH = config('BIOVIZ_BRANCH', default='master')
BIOVIZ_URL = config('BIOVIZ_URL', default='https://bioviz.org/')


# add allowed hosts here
ALLOWED_HOSTS = config('ALLOWED_HOSTS', cast=Csv(), default='localhost')

# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'whoosh',
    'haystack',
    'rest_framework',
    'social_django',
    'apps.apps.AppsConfig',
    'submit_app.apps.SubmitAppConfig',
    'curated_categories.apps.CuratedCategoriesConfig',
    'users.apps.UsersConfig',
    'help.apps.HelpConfig',
    'backend.apps.BackendConfig',
    'download.apps.DownloadConfig',
    'obr.apps.ObrConfig',
    'storages',
]

# Signal Processor for Rebuilding Index

HAYSTACK_CONNECTIONS = {
    'default': {
        'ENGINE': 'haystack.backends.whoosh_backend.WhooshEngine',
        'PATH': os.path.join(os.path.dirname(__file__), 'whoosh_index'),
    },
}

HAYSTACK_SIGNAL_PROCESSOR = 'haystack.signals.RealtimeSignalProcessor'


# https://stackoverflow.com/questions/19256919/location-of-django-logs-and-errors
# https://github.com/ianalexander/ianalexander/blob/master/content/blog/getting-started-with-django-logging-in-5-minutes.html
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'file': {
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'filename': config('DJANGO_LOG_FILE', default=os.path.join(BASE_DIR, 'debug.log')),
        },
    },
    'loggers': {
        'django': {
            'handlers': ['file'],
            'level': 'DEBUG',
            'propagate': True,
        },
    },
}


MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'appstore.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [filejoin(BASE_DIR,'templates'),],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'social_django.context_processors.backends', # Added social django backends processer
                'social_django.context_processors.login_redirect', # < Added social django login redirect processor
            ],
        },
    },
]


AUTHENTICATION_BACKENDS = (
'social_core.backends.google.GoogleOAuth2', # This is for Google OAuth2

 'django.contrib.auth.backends.ModelBackend', # This is to ensure that we are able to login into Django admin
)

# these will probably break eventually
# the callback is localhost
#localhost_key='1021453897231-47d8do1k3ashh6e7vr8fq15lht3up3b7.apps.googleusercontent.com'
localhost_key='75688787985-kdqelrpmlfnmk1v5qgfjp3lf8h7038t1.apps.googleusercontent.com'
#localhost_secret='OfM4FsAIqTiWQmQo8R6OiRlJ'
localhost_secret='35tFEarM2ISnsivKhVU6SjLp'
SOCIAL_AUTH_GOOGLE_OAUTH2_KEY=config('SOCIAL_AUTH_GOOGLE_OAUTH2_KEY',default=localhost_key)
SOCIAL_AUTH_GOOGLE_OAUTH2_SECRET=config('SOCIAL_AUTH_GOOGLE_OAUTH2_SECRET',default=localhost_secret)

LOGIN_URL = '/users/login?next=/'

LOGIN_REDIRECT_URL = '/'
LOGOUT_REDIRECT_URL = '/'

WSGI_APPLICATION = 'appstore.wsgi.application'

# Use in-built django sqllite3 database as default
# django.db.backends.mysql in production
DATABASES = {
    'default': {
        'ENGINE': config('DB_ENGINE',default='django.db.backends.sqlite3'),
        'NAME': config('DB_NAME',default=os.path.join(BASE_DIR, 'db.sqlite3')),
        'USER': config('DB_USER',default="XXXX"),
        'PASSWORD': config('DB_PASSWORD',default="XXXX"), # django user, not rds admin user
        'HOST': config('DB_HOST',default="XXXX"),
        'PORT': '3306',
    }
}

#EMAIL
# used for the from: field in emails
CONTACT_EMAILS = config('CONTACT_EMAILS', cast=Csv(), default="XXXX")
EMAIL_USE_TLS       = True
EMAIL_HOST          = 'smtp.gmail.com'
EMAIL_HOST_USER     = config('EMAIL_HOST_USER',default="XXXX")
EMAIL_ADDR = config('EMAIL_ADDR',default=None)
EMAIL_HOST_PASSWORD = config('EMAIL_HOST_PASSWORD',default="XXXX")
EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_PORT = 587
DEFAULT_FROM_EMAIL = EMAIL_HOST_USER

# Password validation
# https://docs.djangoproject.com/en/2.1/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# Internationalization
# https://docs.djangoproject.com/en/2.1/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# If using AWS S3, a bucket name should be defined in the configuration
# file.
USE_S3=False
try:
    AWS_STORAGE_BUCKET_NAME = config('AWS_STORAGE_BUCKET_NAME')
    USE_S3=True
except UndefinedValueError:
    MEDIA_URL = '/media/'

if USE_S3:
    AWS_S3_CUSTOM_DOMAIN = '%s.s3.amazonaws.com' % AWS_STORAGE_BUCKET_NAME
    AWS_S3_OBJECT_PARAMETERS = {
        'CacheControl': 'no-cache',
    }
    AWS_LOCATION = 'media'
    MEDIA_URL = 'https://%s/%s/' % (AWS_S3_CUSTOM_DOMAIN, AWS_LOCATION)
    DEFAULT_FILE_STORAGE = 'appstore.storage_backends.MediaStorage'
    AWS_DEFAULT_ACL = None

MEDIA_ROOT = os.path.join(BASE_DIR, "media")

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.1/howto/static-files/

MEDIA_ROOT = os.path.join(BASE_DIR, "media")


# site specific settings
# needed for /help/about
GOOGLE_API_KEY=""

# configurations for app_pending_cleanup cron

# Cron string specifies the time when the cleanup process needs to be executed.
# Default string will execute the cron everyday at midnight
# Specify cronstring in settings.ini by referring to link  https://crontab.guru/
# ex. setting to run cron every minute -
# APP_PENDING_CLEANUP_CRONSTRING=*/1 * * * *
APP_PENDING_CLEANUP_CRONSTRING=config('APP_PENDING_CLEANUP_CRONSTRING', default="0 0 * * *")

# This field has value in hours. It will delete entries which are more than 5 hours old
# from the time when cron executes.
APP_PENDING_CLEANUP_TIME_OFFSET=config('APP_PENDING_CLEANUP_TIME_OFFSET', default=5)

#STATIC_ROOT = os.path.join(BASE_DIR, 'static')
STATIC_URL = '/static/'
STATICFILES_DIRS = [
   os.path.join(BASE_DIR, "static"),
]
